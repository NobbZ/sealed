defmodule Sealed.ClientTest do
  use ExUnit.Case, async: false

  alias Sealed.TestHelper.{CLI, Env}
  alias Sealed.{Client, Secret}

  @backends ~w[app-id]

  setup_all do
    Enum.each(@backends, fn backend ->
      CLI.run(:auth, ~w[enable #{backend}])
    end)

    on_exit(fn ->
      Enum.each(@backends, fn backend ->
        CLI.run(:auth, ~w[disable #{backend}])
      end)
    end)
  end

  setup do
    {_, 0} =
      CLI.run(:write, ~w[auth/app-id/map/app-id/app-id], value: "admins", display_name: "app-id")

    {_, 0} = CLI.run(:write, ~w[auth/app-id/map/user-id/user-id], value: "app-id")

    on_exit(fn ->
      {_, 0} = CLI.run(:delete, ~w[auth/app-id/map/user-id/user-id])
      {_, 0} = CLI.run(:delete, ~w[auth/app-id/map/app-id/app-id])
    end)
  end

  test "manual auth" do
    assert {:ok, {:via, Registry, {Client.Registry, :manual}} = cid} =
             Client.new(%Client.Config{vault_addr: Env.uri_struct(), name: :manual})

    assert {:ok, 200, %Secret{} = secret} =
             Client.write(cid, "auth/app-id/login", %{app_id: "app-id", user_id: "user-id"})

    assert {:ok, ttl} = Client.set_token(cid, secret.auth)
    assert is_integer(ttl)

    # TODO: Read a secret

    assert :ok = Client.revoke(cid, :self)
  end

  test "automatic auth" do
    assert {:ok, {:via, Registry, {Client.Registry, :auto}} = cid} =
             Client.new(%Client.Config{vault_addr: Env.uri_struct(), name: :auto})

    assert {:ok, ttl} =
             Client.auth(cid, "auth/app-id/login", %{app_id: "app-id", user_id: "user-id"})

    assert is_integer(ttl)

    # TODO: Read a secret

    assert :ok = Client.revoke(cid, :self)
  end
end
