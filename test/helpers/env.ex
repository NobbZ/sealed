defmodule Sealed.TestHelper.Env do
  @vault_addr "VAULT_ADDR"
  @default_addr "http://localhost:8200"

  @vault_test_token "VAULT_TEST_TOKEN"
  @vault_token "VAULT_TOKEN"
  @default_token "03c72435-412e-58e1-8b50-d4bf9eeba4a1"

  def uri_string(), do: System.get_env(@vault_addr) || @default_addr

  def uri_struct(), do: URI.parse(uri_string())

  def token(),
    do: System.get_env(@vault_test_token) || System.get_env(@vault_token) || @default_token

  def cli_env(), do: [{@vault_addr, uri_string()}, {@vault_token, token()}]
end
