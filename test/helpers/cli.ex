defmodule Sealed.TestHelper.CLI do
  alias Sealed.TestHelper.Env

  def run(cmd, list, values \\ [])

  def run(cmd, args, values) do
    value_list = Enum.map(values, fn {k, v} -> "#{k}=#{v}" end)

    System.cmd("vault", [to_string(cmd) | args ++ value_list], env: Env.cli_env())
  end
end
