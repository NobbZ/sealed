defmodule Sealed.MixProject do
  use Mix.Project

  @gitlab_project "https://gitlab.com/NobbZ/sealed"

  @description """
  A vault client.
  """

  def project do
    [
      app: :sealed,
      version: version(),
      description: @description,
      elixir: "~> 1.6",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :inets],
      mod: {Sealed.Application, []}
    ]
  end

  defp version() do
    case System.cmd("git", ~w[describe]) do
      {description, 0} ->
        description
        |> String.trim()
        |> String.split("-")
        |> case do
          ["v" <> vsn] ->
            vsn

          ["v" <> vsn, commits, "g" <> sha] ->
            "#{vsn}+git.sha.#{sha}.#{commits}"
        end

      _ ->
        {sha, 0} = System.cmd("git", ~w[rev-parse --short HEAD])
        "0.0.0+git.sha.#{String.trim(sha)}"
    end
  end

  defp package() do
    [
      licenses: ["MPL 2.0"],
      links: %{"GitLab" => @gitlab_project}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/helpers"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # Production dependencies
      {:jason, "~> 1.0"},

      # Development dependencies
      {:ex_doc, "~> 0.19.1", only: :dev},
      {:dialyxir, "~> 1.0.0-rc.3", only: [:dev, :test], runtime: false},
      {:mix_test_watch, "~> 0.8", only: :dev, runtime: false}
    ]
  end

  defp docs() do
    [
      name: "Sealed",
      source_url: @gitlab_project,
      homepage_url: @gitlab_project,
      docs: [],
      groups_for_modules: [
        "Data types": [Sealed.Auth, Sealed.Secret, Sealed.Client.Config],
        Others: [Sealed.Client]
      ]
    ]
  end
end
