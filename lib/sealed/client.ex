defmodule Sealed.Client do
  defmodule Config do
    @moduledoc """

    """
    defstruct [:vault_addr, :name]

    @type t :: %__MODULE__{vault_addr: String.t() | URI.t(), name: any}
  end

  use GenServer

  alias Sealed.Secret

  @registry __MODULE__.Registry
  @supervisor __MODULE__.Supervisor

  @typedoc false
  @type state :: %__MODULE__{
          vault_addr: URI.t(),
          token: String.t() | nil,
          auth_data: %{path: String.t(), data: map} | {String.t(), map} | nil
        }

  defstruct vault_addr: %URI{}, token: nil, auth_data: nil

  ## Public API
  @spec new(String.t() | Config.t()) :: {:ok, pid} | {:error, any} | :ignore
  def new(vault_addr) when is_binary(vault_addr), do: new(%Config{vault_addr: vault_addr})

  def new(%Config{} = conf) do
    case {DynamicSupervisor.start_child(@supervisor, {__MODULE__, conf}), conf.name} do
      {{:ok, pid}, nil} -> {:ok, pid}
      {{:ok, _}, name} -> {:ok, {:via, Registry, {@registry, name}}}
      other -> other
    end
  end

  @spec write(pid, String.t(), any) :: {:ok, any} | {:error, any}
  def write(vault, path, data)
  def write(vault, "/" <> path, data), do: write(vault, path, data)

  def write(vault, path, data) do
    GenServer.call(check_cid(vault), {:write, path, data})
  end

  @spec read(pid, String.t()) :: {:ok, Secret.t()} | {:error, any}
  def read(vault, path) do
    GenServer.call(check_cid(vault), {:read, path})
  end

  @spec auth(pid, String.t(), %{String.t() => String.t(), atom => String.t()}) ::
          {:ok, non_neg_integer} | {:error, any}
  def auth(vault, path, data) do
    vault
    |> check_cid()
    |> GenServer.call({:auth, path, data})
  end

  def set_token(vault, %{"client_token" => token}), do: set_token(vault, token)
  def set_token(vault, %{client_token: token}), do: set_token(vault, token)

  def set_token(vault, token) when is_binary(token) do
    GenServer.call(check_cid(vault), {:set_token, token})
  end

  def revoke(vault, token), do: vault |> check_cid() |> GenServer.call({:revoke, token})

  ## GenServer implementation
  def start_link(%Config{name: name} = conf) when name != nil do
    GenServer.start_link(__MODULE__, conf, name: {:via, Registry, {@registry, name}})
  end

  @impl true
  def init(%Config{vault_addr: addr}), do: {:ok, %__MODULE__{vault_addr: addr}}

  @impl true
  def handle_call({:write, path, data}, _from, state) do
    reply = request(:write, path, data, state)

    {:reply, reply, state}
  end

  def handle_call({:read, path}, _from, state) do
    reply = request(:read, path, state)

    {:reply, reply, state}
  end

  def handle_call({:set_token, token}, _from, state) do
    state = %{state | token: token}

    {:ok, 200, secret} = request(:read, "/auth/token/lookup-self", state)

    {:reply, {:ok, secret.data["ttl"]}, state}
  end

  def handle_call({:auth, path, data}, from, state) do
    {:ok, 200, secret} = request(:write, path, data, state)

    {:ok, token} = Secret.token(secret)

    handle_call({:set_token, token}, from, state)
  end

  def handle_call({:revoke, :self}, _from, state) do
    {:ok, 204, nil} = request(:write, "/auth/token/revoke-self", state)

    state = %{state | token: nil}

    {:reply, :ok, state}
  end

  @doc false
  def uri_merge(uri, "/" <> rel), do: uri_merge(uri, rel)

  def uri_merge(uri, rel) do
    uri
    |> URI.merge("v1/")
    |> URI.merge(rel)
    |> to_string()
    |> to_charlist()
  end

  @doc false
  def request(verb, path, data \\ nil, state)

  def request(:read, path, nil, state) do
    url = uri_merge(state.vault_addr, path)

    header = if state.token, do: [{~c"X-Vault-Token", to_charlist(state.token)}], else: []

    req = {url, header}

    with {:ok, {{_, status, _}, _, body}} <- :httpc.request(:get, req, [autoredirect: true], []),
         {:ok, secret} <- Secret.parse(body) do
      {:ok, status, secret}
    end
  end

  def request(:write, path, data, state) do
    url = uri_merge(state.vault_addr, path)

    header = if state.token, do: [{~c"X-Vault-Token", to_charlist(state.token)}], else: []

    body =
      if data do
        data
        |> Jason.encode!()
        |> to_charlist()
      else
        []
      end

    req = {url, header, ~c"application/json", body}

    with {:ok, {{_, status, _}, _, body}} <- :httpc.request(:post, req, [autoredirect: true], []),
         {:ok, secret} <- Secret.parse(body) do
      {:ok, status, secret}
    end
  end

  defp check_cid({:via, _, _} = via), do: via
  defp check_cid(pid) when is_pid(pid), do: pid
  defp check_cid(any), do: any
end
