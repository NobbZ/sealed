defmodule Sealed.Application do
  @moduledoc false

  use Application

  alias Sealed.Client

  @client_registry Client.Registry
  @client_supervisor Client.Supervisor

  # span up the supervision tree, usually no need to call directly by the user of the library
  @doc false
  @spec start(any, any) :: {:ok, pid}
  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: @client_registry},
      {DynamicSupervisor, strategy: :one_for_one, name: @client_supervisor}
    ]

    opts = [strategy: :one_for_one, name: Sealed.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
