defmodule Sealed.Secret do
  alias Sealed.{Auth, WrapInfo}

  defstruct [
    :auth,
    :data,
    :lease_duration,
    :lease_id,
    :renewable,
    :request_id,
    :warnings,
    :wrap_info
  ]

  @typedoc """
  Contains information about any secret as returned by Vault.
  """
  @type t :: %__MODULE__{
          auth: Auth.t(),
          data: %{String.t() => any},
          lease_duration: non_neg_integer,
          lease_id: String.t(),
          renewable: boolean,
          request_id: String.t(),
          warnings: list(String.t()),
          wrap_info: WrapInfo.t()
        }

  @spec token(t | nil) :: {:ok, String.t()} | {:error, atom}
  def token(secret)
  def token(nil), do: {:ok, ""}
  def token(%__MODULE__{auth: %{client_token: <<_::utf8, _::binary>> = token}}), do: {:ok, token}
  def token(%__MODULE__{data: nil}), do: {:ok, ""}
  def token(%__MODULE__{data: %{"id" => nil}}), do: {:ok, ""}
  def token(%__MODULE__{data: %{"id" => token}}) when is_binary(token), do: {:ok, token}
  def token(%__MODULE__{data: %{"id" => _}}), do: {:error, :invalid_data}
  def token(%__MODULE__{}), do: {:ok, ""}

  @doc false
  @spec parse(nil | String.t() | charlist) :: {:ok, nil | t} | {:error, any}
  def parse(body) when body in ["", ~c""], do: {:ok, nil}

  def parse(body) when is_binary(body) or is_list(body) do
    case Jason.decode(body) do
      {:ok, map} -> {:ok, Enum.reduce(map, %__MODULE__{}, &convert/2)}
      {:error, reason} -> {:error, reason}
    end
  end

  defp convert({"auth", data}, secret), do: %{secret | auth: Auth.convert(data)}
  defp convert({"data", data}, secret), do: %{secret | data: data}
  defp convert({"lease_duration", data}, secret), do: %{secret | lease_duration: data}
  defp convert({"lease_id", data}, secret), do: %{secret | lease_id: data}
  defp convert({"renewable", data}, secret), do: %{secret | renewable: data}
  defp convert({"request_id", data}, secret), do: %{secret | request_id: data}
  defp convert({"warnings", data}, secret), do: %{secret | warnings: data}
  defp convert({"wrap_info", data}, secret), do: %{secret | wrap_info: data}
  defp convert(tuple, _secret), do: raise(inspect(tuple))
end
