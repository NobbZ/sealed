defmodule Sealed.Auth do
  defstruct [
    :accessor,
    :client_token,
    :entity_id,
    :identity_policies,
    :lease_duration,
    :metadata,
    :policies,
    :renewable,
    :token_policies
  ]

  @typedoc """
  Contains the auth information if we have it.
  """
  @type t :: %__MODULE__{
          accessor: String.t(),
          client_token: String.t(),
          entity_id: String.t(),
          identity_policies: list(String.t()),
          lease_duration: non_neg_integer,
          metadata: %{String.t() => String.t()},
          policies: list(String.t()),
          renewable: boolean,
          token_policies: list(String.t())
        }

  @doc false
  @spec convert(nil | map) :: nil | t
  def convert(nil), do: nil
  def convert(%{} = map), do: Enum.reduce(map, %__MODULE__{}, &convert/2)

  defp convert({"accessor", data}, auth), do: %{auth | accessor: data}
  defp convert({"client_token", data}, auth), do: %{auth | client_token: data}
  defp convert({"entity_id", data}, auth), do: %{auth | entity_id: data}
  defp convert({"identity_policies", data}, auth), do: %{auth | identity_policies: data}
  defp convert({"lease_duration", data}, auth), do: %{auth | lease_duration: data}
  defp convert({"metadata", data}, auth), do: %{auth | metadata: data}
  defp convert({"policies", data}, auth), do: %{auth | policies: data}
  defp convert({"renewable", data}, auth), do: %{auth | renewable: data}
  defp convert({"token_policies", data}, auth), do: %{auth | token_policies: data}
  defp convert(tuple, _auth), do: raise(inspect(tuple))
end
