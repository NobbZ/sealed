#!/usr/bin/env bash

set -ex

echo "Let vault boot up"
sleep 10

echo "set up app-id"
vault auth enable app-id
vault write auth/app-id/map/app-id/foo value=admins display_name=foo
vault write auth/app-id/map/user-id/bar value=foo

echo "set up app-role"
vault auth enable approle
vault write auth/approle/role/foo-role \
    secret_id_ttl=10m \
    token_num_uses=10 \
    token_ttl=20m \
    token_max_ttl=30m \
    secret_id_num_uses=40

role_id=$(curl --header "X-Vault-Token: ${VAULT_TOKEN}" ${VAULT_ADDR}/v1/auth/approle/role/foo-role/role-id | jq -r .data.role_id)
secret_id=$(curl --header "X-Vault-Token: ${VAULT_TOKEN}" -X POST ${VAULT_ADDR}/v1/auth/approle/role/foo-role/secret-id | jq -r .data.secret_id)

echo "auth with approle"
curl -X POST --data "{\"role_id\":\"${role_id}\",\"secret_id\":\"${secret_id}\"}" ${VAULT_ADDR}/v1/auth/approle/login | jq